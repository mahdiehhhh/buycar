import "@/styles/globals.css";
import { Stack, ThemeProvider, createTheme } from "@mui/material";
import Layout from "@/components/layout/Layout";

const theme = createTheme({
  palette: {
    primary: {
      main: "#afe600",
    },
    NewLightGray: {
      main: "#d0d0cb",
    },
  },
});

export default function App({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ThemeProvider>
  );
}
