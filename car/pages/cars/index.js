import AllCars from "@/components/template/AllCars";
import carsData from "@/components/data/carsData";
import Categories from "@/components/module/CategoriesTab";
import CategoriesTab from "@/components/module/CategoriesTab";
import SearchBar from "@/components/module/SearchBar";
const Cars = () => {
  return (
    <>
      <SearchBar />
      <CategoriesTab />
      <AllCars data={carsData} />
    </>
  );
};

export default Cars;
