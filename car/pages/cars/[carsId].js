import { Stack } from "@mui/material";
import { useRouter } from "next/router";
import carsData from "@/components/data/carsData";
import CarsDetail from "@/components/template/CarsDetail";

const CarDetail = () => {
  const router = useRouter();
  const { carsId } = router.query;
  const carDetail = carsData[carsId - 1];
  return (
    <Stack>
      <CarsDetail {...carDetail} />
    </Stack>
  );
};

export default CarDetail;
