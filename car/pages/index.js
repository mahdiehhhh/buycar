import CategoriesTab from "@/components/module/CategoriesTab";
import SearchBar from "@/components/module/SearchBar";
import AllCars from "@/components/template/AllCars";
import { Container } from "@mui/material";
import carsData from "@/components/data/carsData";

export default function Home() {
  const car = carsData.slice(0, 3);
  return (
    <Container>
      <SearchBar />
      <CategoriesTab />
      <AllCars data={car} />
    </Container>
  );
}
