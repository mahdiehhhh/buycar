import { useRouter } from "next/router";
import carsData from "@/components/data/carsData";
import { Typography } from "@mui/material";
import AllCars from "@/components/template/AllCars";

const SearchFilter = () => {
  const router = useRouter();
  const [min, max] = router.query.slug || [];

  const filterData = carsData.filter(
    (item) => item.price >= min && item.price <= max
  );

  if (!filterData.length)
    return <Typography variant='h3'>Note Found</Typography>;
  return <AllCars data={filterData} />;
};

export default SearchFilter;
