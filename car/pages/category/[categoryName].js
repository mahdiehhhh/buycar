import { useRouter } from "next/router";
import carsData from "@/components/data/carsData";
import { Box, Button, Container, Grid, Stack, Typography } from "@mui/material";
import CarCategory from "@/components/module/CarCategory";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
const Categories = () => {
  const router = useRouter();
  const { categoryName } = router.query;
  const categoryDetail = carsData.filter((cat) => cat.category == categoryName);
  const backHandler = () => {
    router.back();
  };
  return (
    <Container>
      <Stack
        direction='row'
        marginBottom={2}
        alignItems='center'>
        <button
          style={{
            background: "#afe600",
            outline: "none",
            border: "none",
            padding: "5px 10px",
            width: "35px",
            height: "35px",
            borderRadius: "50px",
            display: "flex",
            justifyContent: "center",
            cursor: "pointer",
          }}
          onClick={backHandler}>
          <ArrowBackIcon />
        </button>
        <Typography
          variant='body2'
          color='text.secondary'
          paddingLeft={1}>
          Back
        </Typography>
      </Stack>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        rowSpacing={3}
        columns={{ xs: 4, sm: 8, md: 12 }}>
        {categoryDetail.map((car) => (
          <Grid
            key={car.id}
            item
            xs={2}
            sm={4}
            md={4}>
            <CarCategory {...car} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Categories;
