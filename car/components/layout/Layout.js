import { Box, Typography } from "@mui/material";
import Link from "next/link";

const Layout = ({ children }) => {
  return (
    <>
      <header>
        <Link href='/'>
          <Typography
            variant='h3'
            sx={{
              backgroundColor: "primary.main",
              padding: "10px",
              textAlign: "center",
            }}>
            New Car
          </Typography>
        </Link>
      </header>
      <Box sx={{ marginY: "20px", minHeight: "100vh" }}>{children}</Box>
      <footer>
        <Typography
          variant='h6'
          sx={{
            backgroundColor: "NewLightGray.main",
            textAlign: "center",
            padding: "7px",
          }}>
          Buy your new car | we will help you &copy;
        </Typography>
      </footer>
    </>
  );
};

export default Layout;
