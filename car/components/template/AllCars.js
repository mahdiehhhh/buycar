import { Container, Grid, Stack, Typography } from "@mui/material";
import CarCards from "../module/CarCards";

import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useRouter } from "next/router";

const AllCars = ({ data }) => {
  const router = useRouter();
  const backHandler = () => {
    router.back();
  };
  return (
    <Container>
      <Stack
        direction='row'
        marginBottom={2}
        alignItems='center'>
        <button
          style={{
            background: "#afe600",
            outline: "none",
            border: "none",
            padding: "5px 10px",
            width: "35px",
            height: "35px",
            borderRadius: "50px",
            display: "flex",
            justifyContent: "center",
            cursor: "pointer",
          }}
          onClick={backHandler}>
          <ArrowBackIcon />
        </button>
        <Typography
          variant='body2'
          color='text.secondary'
          paddingLeft={1}>
          Back
        </Typography>
      </Stack>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        rowSpacing={3}
        columns={{ xs: 1, sm: 8, md: 12 }}>
        {data.map((car) => (
          <Grid
            key={car.id}
            item
            xs={2}
            sm={4}
            md={4}>
            <CarCards {...car} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default AllCars;
