import {
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import Image from "next/image";
import ApartmentOutlinedIcon from "@mui/icons-material/ApartmentOutlined";
import TuneOutlinedIcon from "@mui/icons-material/TuneOutlined";
import CalendarTodayOutlinedIcon from "@mui/icons-material/CalendarTodayOutlined";
import AddRoadOutlinedIcon from "@mui/icons-material/AddRoadOutlined";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import AccountBalanceWalletOutlinedIcon from "@mui/icons-material/AccountBalanceWalletOutlined";
const CarsDetail = (props) => {
  const {
    name,
    model,
    year,
    distance,
    location,
    price,
    image,
    id,
    description,
  } = props;
  return (
    <Container>
      <Stack sx={{ display: "table", margin: "auto" }}>
        <img
          src={image}
          alt={name}
          width='100%'
          height='500px'
          style={{ borderRadius: "7px" }}
        />
        <Typography
          variant='h5'
          mt={3}>
          {name} {model}
        </Typography>
        <Card sx={{ maxWidth: 700, marginTop: "20px" }}>
          <CardContent>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                marginBottom: "10px",
              }}>
              <Typography sx={{ display: "flex", alignItems: "center" }}>
                <ApartmentOutlinedIcon sx={{ paddingRight: "5px" }} /> company
              </Typography>

              <Typography>{name}</Typography>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                marginBottom: "10px",
              }}>
              <Typography sx={{ display: "flex", alignItems: "center" }}>
                <TuneOutlinedIcon sx={{ paddingRight: "5px" }} /> model
              </Typography>

              <Typography>{model}</Typography>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                marginBottom: "10px",
              }}>
              <Typography sx={{ display: "flex", alignItems: "center" }}>
                <CalendarTodayOutlinedIcon sx={{ paddingRight: "5px" }} /> first
                registration
              </Typography>

              <Typography>{year}</Typography>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                marginBottom: "10px",
              }}>
              <Typography sx={{ display: "flex", alignItems: "center" }}>
                <AddRoadOutlinedIcon sx={{ paddingRight: "5px" }} />
                kms Driven
              </Typography>

              <Typography>{distance}</Typography>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                marginBottom: "10px",
              }}>
              <Typography sx={{ display: "flex", alignItems: "center" }}>
                <LocationOnOutlinedIcon sx={{ paddingRight: "5px" }} />
                location
              </Typography>

              <Typography>{location}</Typography>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}>
              <Typography sx={{ display: "flex", alignItems: "center" }}>
                <AccountBalanceWalletOutlinedIcon
                  sx={{ paddingRight: "5px" }}
                />
                price
              </Typography>

              <Typography>$ {price}</Typography>
            </Box>
          </CardContent>
        </Card>
        <Card sx={{ maxWidth: 700, marginTop: "20px" }}>
          <CardContent>
            <Typography
              variant='body1'
              color='text.secondary'>
              {description}
            </Typography>
          </CardContent>
        </Card>

        <Button
          variant='contained'
          sx={{ background: "primary.main", width: "100%", marginTop: "20px" }}>
          Buy
        </Button>
      </Stack>
    </Container>
  );
};

export default CarsDetail;
