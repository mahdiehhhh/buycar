import { Card, CardContent, CardMedia, Stack, Typography } from "@mui/material";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
const CarCategory = (props) => {
  const { name, model, year, distance, location, price, image } = props;
  return (
    <>
      <Card sx={{ padding: "10px" }}>
        <CardMedia
          title='car image'
          component='img'
          image={image}
          sx={{ height: "200px" }}
        />

        <CardContent>
          <Typography
            gutterBottom
            variant='h6'
            component='h6'>
            {`${name} ${model}`}
          </Typography>
          <Typography
            variant='body2'
            color='text.secondary'>
            {`${year} . ${distance}km`}
          </Typography>
        </CardContent>

        <Stack
          direction='row'
          justifyContent='space-between'
          paddingX={2}>
          <Typography
            sx={{
              backgroundColor: "primary.main",
              paddingX: "5px",
              borderRadius: "5px",
            }}>
            $ {price}
          </Typography>
          <Typography sx={{ display: "flex", alignItems: "center" }}>
            {location}
            <LocationOnOutlinedIcon />
          </Typography>
        </Stack>
      </Card>
    </>
  );
};

export default CarCategory;
