import {
  Box,
  Button,
  Container,
  Grid,
  Slider,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";
import { useState } from "react";

const SearchBar = () => {
  const router = useRouter();
  const [value, setValue] = useState([0, 50000]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const searchHandler = () => {
    router.push(`/filter/${value[0]}/${value[value.length - 1]}`);
  };
  return (
    <Container>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        rowSpacing={3}
        columns={{ xs: 4, sm: 8, md: 18 }}
        marginY={3}
        padding={2}
        display='flex'
        justifyContent='Center'
        alignItems='center'>
        <Grid item>
          <Typography variant='h5'>
            price ${value[0]} until ${value[value.length - 1]}
          </Typography>
          <Slider
            getAriaLabel={() => "Temperature range"}
            value={value}
            onChange={handleChange}
            valueLabelDisplay='auto'
            min={0}
            max={50000}
          />
        </Grid>
        <Grid item>
          <Button
            variant='contained'
            onClick={searchHandler}>
            Search
          </Button>
        </Grid>
      </Grid>
    </Container>
  );
};

export default SearchBar;
