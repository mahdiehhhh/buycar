import { Box, Container, Grid, Stack, Typography } from "@mui/material";
import DirectionsCarFilledOutlinedIcon from "@mui/icons-material/DirectionsCarFilledOutlined";
import { useRouter } from "next/router";
import Link from "next/link";
import carsData from "../data/carsData";

const CategoriesTab = () => {
  const filterCategory = carsData.filter((cat) => cat.category);
  return (
    <Container>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        rowSpacing={3}
        columns={{ xs: 4, sm: 8, md: 18 }}
        marginY={3}
        padding={2}
        display='flex'
        justifyContent='Center'>
        <Grid
          item
          xs={2}
          sm={2}
          md={3}>
          <Link href='/category/sedan'>
            <Box
              sx={{
                backgroundColor: "primary.main",
                padding: "5px 10px",
                borderRadius: "5px",
                width: "98px",
              }}>
              <Typography textAlign='center'>Sedan</Typography>
              <Typography textAlign='center'>
                <DirectionsCarFilledOutlinedIcon fontSize='small' />
              </Typography>
            </Box>
          </Link>
        </Grid>

        <Grid
          item
          xs={2}
          sm={2}
          md={3}>
          <Link href='/category/suv'>
            <Box
              sx={{
                backgroundColor: "primary.main",
                padding: "5px 10px",
                borderRadius: "5px",
                width: "98px",
              }}>
              <Typography textAlign='center'>Suv</Typography>
              <Typography textAlign='center'>
                <DirectionsCarFilledOutlinedIcon fontSize='small' />
              </Typography>
            </Box>
          </Link>
        </Grid>

        <Grid
          item
          xs={2}
          sm={2}
          md={3}>
          <Link href='/category/hatchback'>
            <Box
              sx={{
                backgroundColor: "primary.main",
                padding: "5px 10px",
                borderRadius: "5px",
                width: "98px",
              }}>
              <Typography textAlign='center'>HatchBack</Typography>
              <Typography textAlign='center'>
                <DirectionsCarFilledOutlinedIcon fontSize='small' />
              </Typography>
            </Box>
          </Link>
        </Grid>
        <Grid
          item
          xs={2}
          sm={2}
          md={3}>
          <Link href='/category/sport'>
            <Box
              sx={{
                backgroundColor: "primary.main",
                padding: "5px 10px",
                borderRadius: "5px",
                width: "98px",
              }}>
              <Typography textAlign='center'>Sport</Typography>
              <Typography textAlign='center'>
                <DirectionsCarFilledOutlinedIcon fontSize='small' />
              </Typography>
            </Box>
          </Link>
        </Grid>
      </Grid>
    </Container>
  );
};

export default CategoriesTab;
